apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "am-hg.resource.prefix" . }}-am-cp-1-deployment
  namespace: {{ .Release.Namespace }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      deployment: {{ template "am-hg.resource.prefix" . }}-am-cp
      node: {{ template "am-hg.resource.prefix" . }}-am-cp-1
  template:
    metadata:
      annotations:
        checksum.am.conf: {{ include (print $.Template.BasePath "/am/control-plane/instance-1/wso2am-hg-am-control-plane-conf.yaml") . | sha256sum }}
      labels:
        deployment: {{ template "am-hg.resource.prefix" . }}-am-cp
        node: {{ template "am-hg.resource.prefix" . }}-am-cp-1
        product: apim
    spec:
      initContainers:
        - name: init-db
          image: busybox:1.32
          command: [ 'sh', '-c', 'echo -e "Checking for the availability of DB Server deployment"; while ! nc -z "{{ .Values.wso2.deployment.am.db.hostname }}" {{ .Values.wso2.deployment.am.db.port }}; do sleep 1; printf "-"; done; echo -e "  >> DB Server has started";' ]
        - name: init-db-connector-download
          image: busybox:1.32
          command:
            - /bin/sh
            - "-c"
            - |
              set -e
              connector_version=8.0.17
              wget "{{ .Values.wso2.deployment.am.db.driver_url }}" -P /db-connector-jar/
          volumeMounts:
            - name: db-connector-jar
              mountPath: /db-connector-jar
      containers:
        - name: wso2am
{{- include "image" (dict "Values" .Values "deployment" .Values.wso2.deployment.am) | indent 10 }}
          imagePullPolicy: {{ .Values.wso2.deployment.am.imagePullPolicy }}
          livenessProbe:
            httpGet:
              path: /services/Version
              port: 9763
            initialDelaySeconds: {{ .Values.wso2.deployment.am.cp.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.wso2.deployment.am.cp.livenessProbe.periodSeconds }}
          readinessProbe:
            httpGet:
              path: /services/Version
              port: 9763
            initialDelaySeconds: {{ .Values.wso2.deployment.am.cp.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.wso2.deployment.am.cp.readinessProbe.periodSeconds }}
          lifecycle:
            postStart:
              exec:
                command:
                  - sh
                  - -c
                  - for file in $(find ${WSO2_SERVER_HOME}/repository/deployment -name "*_callback.jsp"); do sed -i 's/sendRedirect(context/sendRedirect(org.wso2.carbon.apimgt.impl.utils.APIUtil.getServerURL() + context/g' $file; done
            preStop:
              exec:
                command:  ['sh', '-c', '${WSO2_SERVER_HOME}/bin/api-manager.sh stop']
          resources:
            requests:
              memory: {{ .Values.wso2.deployment.am.cp.resources.requests.memory }}
              cpu: {{ .Values.wso2.deployment.am.cp.resources.requests.cpu }}
            limits:
              memory: {{ .Values.wso2.deployment.am.cp.resources.limits.memory }}
              cpu: {{ .Values.wso2.deployment.am.cp.resources.limits.cpu }}
          securityContext:
            runAsUser: 802
          ports:
            - containerPort: 8280
              protocol: "TCP"
            - containerPort: 8080
              protocol: "TCP"
            - containerPort: 8243
              protocol: "TCP"
            - containerPort: 9763
              protocol: "TCP"
            - containerPort: 9443
              protocol: "TCP"
            - containerPort: 9711
              protocol: "TCP"
            - containerPort: 9611
              protocol: "TCP"
            - containerPort: 5672
              protocol: "TCP"
          env:
            - name: PROFILE_NAME
              value: control-plane
            - name: NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: JVM_MEM_OPTS
              value: "-Xms{{ .Values.wso2.deployment.am.resources.jvm.heap.memory.xms }} -Xmx{{ .Values.wso2.deployment.am.resources.jvm.heap.memory.xmx }}"
          volumeMounts:
            - name: wso2am-conf
              mountPath: /home/wso2carbon/wso2-config-volume/repository/conf
            {{ if .Values.wso2.deployment.persistentRuntimeArtifacts.apacheSolrIndexing.enabled }}
            - name: wso2am-local-carbon-database-storage
              mountPath: /home/wso2carbon/solr/database
            - name: wso2am-solr-indexed-data-storage
              mountPath: /home/wso2carbon/solr/indexed-data
            - name: wso2am-conf-entrypoint
              mountPath: /home/wso2carbon/docker-entrypoint.sh
              subPath: docker-entrypoint.sh
            {{ end }}
            - name: db-connector-jar
              mountPath: /home/wso2carbon/wso2-artifact-volume/repository/components/lib
            - name: nfs-pv
              mountPath: "/nfsshare"
      serviceAccountName: {{ .Values.kubernetes.serviceAccount }}
      {{- if .Values.wso2.deployment.am.imagePullSecrets }}
      priorityClassName: high-priority
      imagePullSecrets:
        - name: {{ .Values.wso2.deployment.am.imagePullSecrets }}
      {{- else if and (not (eq .Values.wso2.subscription.username "")) (not (eq .Values.wso2.subscription.password "")) }}
      imagePullSecrets:
        - name: {{ template "am-hg.resource.prefix" . }}-wso2-private-registry-creds
      {{ end }}
      volumes:
        - name: wso2am-conf
          configMap:
            name: {{ template "am-hg.resource.prefix" . }}-am-cp-1-conf
        {{ if .Values.wso2.deployment.persistentRuntimeArtifacts.apacheSolrIndexing.enabled }}
        - name: wso2am-local-carbon-database-storage
          persistentVolumeClaim:
            claimName: {{ template "am-hg.resource.prefix" . }}-am-cp-1-local-carbon-database-volume-claim
        - name: wso2am-solr-indexed-data-storage
          persistentVolumeClaim:
            claimName: {{ template "am-hg.resource.prefix" . }}-am-cp-1-solr-indexed-data-volume-claim
        - name: wso2am-conf-entrypoint
          configMap:
            name: {{ template "am-hg.resource.prefix" . }}-am-cp-conf-entrypoint
            defaultMode: 0407
        {{ end }}
        - name: nfs-pv
          persistentVolumeClaim:
            claimName: nfs-pv-claim
        - name: db-connector-jar
          emptyDir: {}
